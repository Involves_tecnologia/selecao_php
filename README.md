Seleção Desenvolvedor PHP (pleno)
==

Olá, estamos muito felizes pelo seu interesse em fazer parte da nossa equipe. 
Gostaríamos de conhecer um pouco mais das suas habilidades como desenvolvedor PHP e, para isso, preparamos o teste abaixo.

Sobre o teste:
--

Criar um gerenciamento aonde seja possível criar, listar e visualizar um usuário.
Atributos de um usuário são:

nome, email, senha, data de nascimento, foto

O sistema deverá ter:
--

Tela de login e cadastro onde devemos ter:
--

- Formulário de login com os campos email e senha
- Formulário de cadastro com os todos os atributos do usuário obrigatórios

E uma tela de home onde devemos ter:
--

- A listagem dos usuários com paginação, trazendo os usuários mais recentes primeiro
- A exibição do usuário na listagem deve conter nome e data de nascimento
- Ao clicar no usuário, redirecionar para uma página que carregue todos os dados do mesmo, exceto senha

Autenticação:
--
Restringir o acesso à listagem de usuários e página do usuário.


Observações:
--
- Você poderá utilizar php "puro" ou um framework da sua preferência
- No front-end você poderá usar a tecnologia da sua preferência 
- A persistência de dados deverá ser feita em um banco relacional


Para fins de avaliação, será levado em consideração os seguintes quesitos:
--
- Utilização de orientação a objetos e boas práticas de programação.
- Utilização das PSRs (PHP Standards Recommendations).


Diferenciais: 
--
- Disponibilizar a aplicação on-line (heroku, digital ocean ou qualquer serviço de sua preferência).

Envio:
--
- Você pode enviar um zip do seu projeto contendo o código fonte ou link do repositório para talentos@involves.com.br com o título "[teste-php-2017-mes] <nome_do_candidato>" até a data limite informada no email em que você recebeu o link para esse teste.